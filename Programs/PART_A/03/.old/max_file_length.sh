max_file_length() {
	echo $#
    if [ $# -ne 1 ]; then
        echo "Usage: max_file_length <directory>"
        return 1
    fi

    local directory="$1"
    local max_depth=0

    if [ ! -d "$directory" ]; then
        echo "Error: '$directory' is not a valid directory."
        return 1
    fi

    # Find all files recursively and get their sizes
    while IFS= read -r -d '' file; do
    	echo $file
    	depth=$(echo $file | grep -o "/" | wc -l)
    	echo $depth
        if  [ -n "$depth" ] &&[ "$depth" -gt "$max_depth" ]; then
        	max_depth="$depth"
        fi
    done < <(find "$directory" -type d -print0)

    echo "Maximum length of any file in '$directory' hierarchy: $max_depth"
}

max_file_length $1
