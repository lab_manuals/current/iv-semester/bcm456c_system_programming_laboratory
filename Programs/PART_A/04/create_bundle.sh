#!/bin/bash

# Check if at least one argument is provided
if [ $# -lt 1 ]; then
    echo "Usage: $0 <file1> [<file2> ...]"
    exit 1
fi

# Get the output filename for the bundle script
bundle_script="recreate_files.sh"

# Check if the bundle script already exists
if [ -e "$bundle_script" ]; then
    echo "Error: Bundle script '$bundle_script' already exists."
    exit 1
fi

# Create the bundle script and set permissions
echo "#!/bin/bash" > "$bundle_script"
echo "" >> "$bundle_script"
echo "# Script to recreate files" >> "$bundle_script"
echo "" >> "$bundle_script"

# Loop through each file and append its contents and creation code to the bundle script
for file in "$@"; do
    echo "" >> "$bundle_script"
    echo "# Content of file: $file" >> "$bundle_script"
    echo "cat <<'EOF' > \"$file\"" >> "$bundle_script"
    cat "$file" >> "$bundle_script"
    echo "EOF" >> "$bundle_script"
done

# Make the bundle script executable
chmod +x "$bundle_script"

echo "Bundle script created: $bundle_script"

