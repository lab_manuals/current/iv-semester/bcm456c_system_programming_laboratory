#!/bin/bash

# Script to recreate files


# Content of file: f1.txt
cat <<'EOF' > "f1.txt"
Q:	What's the difference between the 1950's and the 1980's?
A:	In the 80's, a man walks into a drugstore and states loudly, "I'd
	like some condoms," and then, leaning over the counter, whispers,
	"and some cigarettes."
EOF

# Content of file: f2.txt
cat <<'EOF' > "f2.txt"
I've touch'd the highest point of all my greatness;
And from that full meridian of my glory
I haste now to my setting.  I shall fall,
Like a bright exhalation in the evening
And no man see me more.
		-- Shakespeare
EOF

# Content of file: f3.txt
cat <<'EOF' > "f3.txt"
Q:	What's tan and black and looks great on a lawyer?
A:	A doberman.
EOF
