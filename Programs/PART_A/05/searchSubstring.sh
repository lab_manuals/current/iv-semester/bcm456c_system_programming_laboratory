#!/bin/bash

# Accept string and substring from the user
echo "Enter a string:"
read string
echo "Enter a substring:"
read substring

# Check if the string contains the substring
if [[ "$string" == *"$substring"* ]]; then
    echo "The string '$string' contains the substring '$substring'."
else
    echo "The string '$string' does not contain the substring '$substring'."
fi

