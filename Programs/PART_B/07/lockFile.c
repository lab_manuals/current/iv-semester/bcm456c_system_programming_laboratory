// lock_file.c
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

int main() {
    int fd;
    struct flock fl;

    // Open the file
    if ((fd = open("demo.txt", O_RDWR)) == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    // Set up lock information
    fl.l_type = F_WRLCK;  // Exclusive lock
    fl.l_whence = SEEK_END;
    fl.l_start = -100; // Lock the last 100 bytes
    fl.l_len = 100;

    // Try to acquire the lock
    if (fcntl(fd, F_SETLK, &fl) == -1) {
        perror("fcntl");
        exit(EXIT_FAILURE);
    }

    printf("File locked by process with PID %d\n", getpid());

    // Keep the file locked for a while
    sleep(10);

    // Close the file
    if (close(fd) == -1) {
        perror("close");
        exit(EXIT_FAILURE);
    }

    return 0;
}

