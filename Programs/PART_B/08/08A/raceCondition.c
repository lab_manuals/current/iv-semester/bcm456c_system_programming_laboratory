#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

int global_variable = 0;

void *increment(void *arg) {
    for (int i = 0; i < 10000; ++i) {
        global_variable++;
    }
    return NULL;
}

int main() {
    pthread_t tid1, tid2;

    // Create two threads
    pthread_create(&tid1, NULL, increment, NULL);
    pthread_create(&tid2, NULL, increment, NULL);

    // Wait for threads to finish
    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);

    printf("Global variable value: %d\n", global_variable);

    return 0;
}

