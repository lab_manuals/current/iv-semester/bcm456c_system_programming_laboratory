#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <source> <link_name>\n", argv[0]);
        return 1;
    }

	int iOption;
	printf("\n1.Hard Link\n2.Symbolic Link\nChoose an option : ");
	scanf("%d", &iOption);
	
	if(iOption == 1)
	{
    // Create a hard link
		if (link(argv[1], argv[2]) != 0) {
		    perror("Error creating hard link");
		    return 1;
		}	
		printf("Hard link created: %s -> %s\n", argv[2], argv[1]);
	}
    
	if(iOption == 2)
	{
    // Create a symbolic link
		if (symlink(argv[1], strcat(argv[2], "_symlink")) != 0) {
		    perror("Error creating symbolic link");
		    return 1;
		}

	    printf("Symbolic link created: %s -> %s\n", argv[2], argv[1]);
	}
    return 0;
}

