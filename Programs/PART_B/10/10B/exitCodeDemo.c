#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    int a,b, res;

/*	printf("argument count = %d\n", argc);*/
    if(argc == 3)
    {
    	a = atoi(argv[1]);
    	b = atoi(argv[2]);
		if(b != 0)
		{
			res = a / b;
			printf("\nResult of %d / %d = %d\n", a, b, res);
			// Exiting with status 0 (success)
			printf("Exiting with status 0 (success)\n");
			exit(EXIT_SUCCESS);		//		exit(0);
		
		}
		if (b == 0)
		{
			// Exiting with status 1 (general error)
			printf("\nInvalid Division Operation\n");
			printf("Exiting with status 1 (general error)\n");
			exit(EXIT_FAILURE);		//		exit(1);
		}
		
    }

    // Exiting with status 2 (incorrect usage)		here user can specify his own exit codes
    printf("Exiting with status 2 (incorrect usage)\n");
    exit(2);

    return 0;
}

