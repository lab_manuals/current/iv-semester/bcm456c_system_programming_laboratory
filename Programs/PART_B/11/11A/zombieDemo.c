#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main() {
    pid_t child_pid;

    // Fork a child process
    child_pid = fork();

    if (child_pid < 0) {
        // Fork failed
        perror("Fork failed");
        return 1;
    } else if (child_pid == 0) {
        // Child process
        printf("Child process (PID: %d) is running.\n", getpid());
        // Child process exits immediately without calling exit
    } else {
        // Parent process
        printf("Parent process (PID: %d) created a child process with PID: %d.\n", getpid(), child_pid);
        sleep(2); // Wait for child process to become a zombie
        printf("\nPID\tPPID\tSTATE\tName\n");
        system("ps -eo pid,ppid,stat,cmd | grep Z+"); // Execute ps command to check for zombie process
        printf("\nHere Zombie processes have their state mentioned as Z+\n");
    }

	
    return 0;
}

