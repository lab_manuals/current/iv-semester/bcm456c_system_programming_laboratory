#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main() 
{
    pid_t child_pid, grandchild_pid;

    // First fork to create a child process
    child_pid = fork();

    if (child_pid < 0) 
	{
        // Fork failed
        perror("Fork failed");
        return 1;
    } 
    else if (child_pid == 0) 
	{
        // Child process
        // Second fork to create a grandchild process
        grandchild_pid = fork();

        if (grandchild_pid < 0) 
		{
            // Fork failed
            perror("Second fork failed");
            return 1;
        } 
        else if (grandchild_pid == 0) 
		{
            // Grandchild process
            printf("Grandchild process (PID: %d) is running.\n", getpid());
            // Grandchild process exits
            exit(0);
        } 
        else 
		{
            // Child process exits immediately without waiting
            printf("Child process (PID: %d) created a grandchild process with PID: %d.\n", getpid(), grandchild_pid);
            exit(0);
        }
    } 
    else 
	{
        // Parent process
        printf("Parent process (PID: %d) created a child process with PID: %d.\n", getpid(), child_pid);
        // Parent process waits for child to terminate
        wait(NULL);
        printf("Parent process exiting.\n");
    }

    return 0;
}

